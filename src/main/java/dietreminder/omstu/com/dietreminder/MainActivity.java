package dietreminder.omstu.com.dietreminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * This class contains methods for application run.
 */
public class MainActivity extends Activity {
    /**
     * This button for creating of diet.
     */
    private Button loseDietButton;
    /**
     * This button for deleting of diet.
     */
    private Button ganeDietButton;


    /**
     * Application initializer.
     * @param savedInstanceState of Bundle type
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loseDietButton = (Button) findViewById(R.id.loseweight_button);
        ganeDietButton = (Button) findViewById(R.id.gainweight_button);
    }

    /**
     * Lose diet click.
     * @param view of View
     */
    public void loseDietClick(View view) {
        Intent intent = new Intent(MainActivity.this, SelectDietLose.class);
        startActivity(intent);
    }

    /**
     * Gane diet click.
     * @param view of View
     */
    public void gainDietClick(View view) {
        Intent intent = new Intent(MainActivity.this, SelectDietGane.class);
        startActivity(intent);
    }
}
