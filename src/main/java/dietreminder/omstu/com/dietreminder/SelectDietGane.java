package dietreminder.omstu.com.dietreminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class contains methods to display of diets list.
 */
public class SelectDietGane extends Activity {
    /**
     * Array diet list.
     */
    private String[] diets;
    /**
     * This object contains of title list.
     */
    private ListView listInfoGane;
    /**
     * Contains title from documents.
     */
    private ArrayList<String> titleList;
    /**
     * Array adapter.
     */
    private ArrayAdapter<String> adapter;

    /**
     * Initialization activity.
     * @param savedInstanceState of Bundle type
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_diet_gane);
        listInfoGane = (ListView) findViewById(R.id.title_list_gane);
        titleList = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, R.layout.buffer, R.id.text_view, titleList);
        listInfoGane.setAdapter(adapter);
        listInfoGane.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SelectDietGane.this, ShowDietActivity.class);
                intent.putExtra("fname", String.valueOf(adapter.getItem(position)));
                startActivity(intent);
            }
        });
        loadTitleText();
    }

    /**
     * Load text from ListView.
     */
    public void loadTitleText() {
        diets = getResources().getStringArray(R.array.diets_gane);
        Collections.addAll(titleList, diets);
    }
}
