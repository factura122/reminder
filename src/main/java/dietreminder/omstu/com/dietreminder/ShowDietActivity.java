package dietreminder.omstu.com.dietreminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class realised show text from file.
 */
public class ShowDietActivity extends Activity {
    String readString;
    /**
     * TextView initializer.
     */
    private TextView textDiet;
    /**
     * Start diet button.
     */
    private Button startDietButton;
    /**
     * Load time info.
     */
    private String[] extractData;
    /**
     * List of time.
     */
    private LinkedList<String> timeList;

    /**
     * Initialization activity.
     *
     * @param savedInstanceState of Bundle type
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_diet);
        textDiet = (TextView) findViewById(R.id.text_diet);
        startDietButton = (Button) findViewById(R.id.start_diet_button);
        Intent intent = getIntent();
        final String format = ".txt";
        final String fname = intent.getStringExtra("fname") + format;
        readFile(fname);
    }

    public void readFile(String path) {
        try {
            InputStream streamInFile = getAssets().open(path);
            int size = streamInFile.available();
            byte[] buffer = new byte[size];
            streamInFile.read(buffer);
            streamInFile.close();
            readString = new String(buffer);
            textDiet.setText(readString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Extract time.
     */
    public void extractTime() {
        timeList = new LinkedList<>();
        String text = textDiet.getText().toString();
        String[] timesBuffer = new String[text.length()];
        String result = "";
        int index = 0;
        Matcher regexData = Pattern.compile("\\((?:([[\\d:]]+))\\)").matcher(text);
        while (regexData.find()) {
            timesBuffer[index] = regexData.group();
            result += timesBuffer[index];
            index++;
        }
        extractData = result.split("([^\\d:])");
        for (String time : extractData) {
            if (!time.equals("")) {
                timeList.add(time);
            }
        }
    }

    public String[] getText() {
        return extractData;
    }

    /**
     * Add diet.
     *
     * @param view of View
     */
    public void selectDietClick(View view) {
        extractTime();
        Calendar calendar = Calendar.getInstance();
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intentAlarm = new Intent(this, TimeNotification.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intentAlarm,
                PendingIntent.FLAG_UPDATE_CURRENT);
        manager.cancel(pendingIntent);
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 13);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 19);
        calendar.set(Calendar.MINUTE, 0);
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        Toast.makeText(getApplicationContext(), "Вы начали диету", Toast.LENGTH_SHORT).show();
    }
}

