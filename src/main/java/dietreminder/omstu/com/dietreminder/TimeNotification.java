package dietreminder.omstu.com.dietreminder;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.support.v4.app.NotificationCompat.VISIBILITY_PUBLIC;

/**
 * Timer notification.
 */

public class TimeNotification extends BroadcastReceiver {
    /**
     * Notification manager.
     */
    private NotificationManager pushMessage;

    /**
     * Configuration notification.
     * @param context of Application context
     * @param intent of Intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        pushMessage = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notif = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle("Время принимать пищу!")
                .setContentText("Иначе умрете от истощения!")
                .setTicker("Примите пищу или вам остался месяц!")
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(new long[] {1000, 1000, 1000, 5000, 5000})
                .setLights(Color.RED, 0, 1)
                .setAutoCancel(true);
        TaskStackBuilder builder = TaskStackBuilder.create(context);
        builder.addParentStack(ViewDiet.class);
        builder.addNextIntent(intent);
        PendingIntent resultPI = builder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notif.setContentIntent(resultPI);
        notif.setVisibility(VISIBILITY_PUBLIC);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, pendingIntent);
        pushMessage.notify(0, notif.build());
    }
}
