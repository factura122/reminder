package dietreminder.omstu.com.dietreminder;

import android.content.Intent;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "src/main/AndroidManifest.xml", sdk = 21)
public class MainActivityTest {
    private MainActivity main;

    @Before
    public void setUp() {
        main = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() {
        assertNotNull(main);
    }

    @Test
    public void transitionToLoseDiet() {
        Button button = (Button) main.findViewById(R.id.loseweight_button);
        button.performClick();
        Intent intent = Shadows.shadowOf(main).peekNextStartedActivity();
        assertEquals(SelectDietLose.class.getCanonicalName(),
                intent.getComponent().getClassName());
    }

    @Test
    public void transitionToGaneDiet() {
        Button button = (Button) main.findViewById(R.id.gainweight_button);
        button.performClick();
        Intent intent = Shadows.shadowOf(main).peekNextStartedActivity();
        assertEquals(SelectDietGane.class.getCanonicalName(),
                intent.getComponent().getClassName());
    }
}
