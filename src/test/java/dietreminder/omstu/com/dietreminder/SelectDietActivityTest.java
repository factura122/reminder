package dietreminder.omstu.com.dietreminder;

import android.content.Intent;
import android.widget.ListView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "src/main/AndroidManifest.xml", sdk = 21)
public class SelectDietActivityTest {
    private SelectDietGane gane;
    private SelectDietLose lose;

    @Before
    public void setUp() {
        gane = Robolectric.buildActivity(SelectDietGane.class)
                .create()
                .resume()
                .get();
        lose = Robolectric.buildActivity(SelectDietLose.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() {
        assertNotNull(gane);
        assertNotNull(lose);
    }

    @Test
    public void ganeClick() {
        ListView listView = (ListView) gane.findViewById(R.id.title_list_gane);
        listView.performItemClick(listView.getAdapter().getView(0, null, null), 0,
                listView.getAdapter().getItemId(0));
        Intent intent = Shadows.shadowOf(gane).peekNextStartedActivity();
        assertEquals(ShowDietActivity.class.getCanonicalName(),
                intent.getComponent().getClassName());
    }

    @Test
    public void loseClick() {
        ListView listLose = (ListView) lose.findViewById(R.id.title_list);
        listLose.performItemClick(listLose.getAdapter().getView(1, null, null), 1,
                listLose.getAdapter().getItemId(1));
        Intent intent = Shadows.shadowOf(lose).peekNextStartedActivity();
        assertEquals(ShowDietActivity.class.getCanonicalName(),
                intent.getComponent().getClassName());
    }
}
